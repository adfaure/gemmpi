{
  kapack ? import
    (builtins.fetchTarball {
      url = "https://github.com/oar-team/kapack/archive/master.tar.gz";
      # Latest build: commit: 4590972fc39a856de3fcca21280b195cf8521bc7
    }) {}
}:

with kapack;
with pkgs;
with stdenv.lib;

let blas64 = null; in
let blas64_ = blas64; in

let
  # To add support for a new platform, add an element to this set.
  configs = {
    # For my computer
    x86_64-linux = {
      BINARY = "64";
      TARGET = "ATHLON";
      DYNAMIC_ARCH = "1";
      CC = "gcc";
      # I disable openmp so I can ensure that  the application is single threaded.
      USE_OPENMP ="0";
    };
  };

  config =
    configs.${stdenv.hostPlatform.system}
    or (throw "unsupported system: ${stdenv.hostPlatform.system}");

  blas64 =
    if blas64_ != null
      then blas64_
      else hasPrefix "x86_64" stdenv.hostPlatform.system;

  custom_openblas = (openblas.overrideDerivation (attrs: rec {
    platform = stdenv.hostPlatform.system;
    preConfigure="echo bro: $platform";
    makeFlags =
      [
        "FC=gfortran"
        ''PREFIX="''$(out)"''
        "USE_THREAD=0"
        "NUM_THREADS=64"
        "INTERFACE64=${if blas64 then "1" else "0"}"
        "NO_STATIC=1"
      ] ++ stdenv.lib.optional (stdenv.hostPlatform.libc == "musl") "NO_AFFINITY=1"
      ++ stdenv.lib.mapAttrsToList (var: val: var + "=" + val) config;
  }));

  ezTrace = pkgs.stdenv.mkDerivation {
    name = "ezTrace";
    src = fetchgit {
      url = "https://scm.gforge.inria.fr/anonscm/git/eztrace/eztrace.git";
      rev = "c1a5a14dc411e44218425906eefa6d3680c5be8f";
      sha256 = "18wal94g0x0zmw9qkqmyvywri7ijwg5827x7ijkl0vz8kxwa2nwm";
    };
    preConfigure = "./bootstrap";
    configureFlags = [
      "--with-mpi=${pkgs.openmpi}"
    ];
    buildInputs = [
      automake
      autoconf
      libtool
      libelf
      libbfd
      gfortran
      libopcodes
      openmpi
    ];
  };

  akypuera = pkgs.stdenv.mkDerivation {
    name = "ezTrace";
    src = fetchgit {
      url = "https://github.com/schnorr/akypuera";
      rev = "9fb5a3dddef8a91f71e18032db9cbb8d2c8cf8e1";
      sha256 = "0k8gyyjl31p777llxd5i0bj0by8x878imkyx3pqln3lbca48368j";
    };
    buildInputs = [
      cmake
      openmpi
    ];
  };

  shells = rec {
    inherit pkgs;
    smpi = pkgs.mkShell {
      buildInputs = [
        (scalapack.overrideAttrs (oldAttrs: rec {
          postInstall =  ''
            ls -l;
          '';
          doCheck = false;
        }))
        gfortran
        ezTrace
        # kapack.simgrid
        # (kapack.simgrid.overrideAttrs (old: rec {
        #   src = fetchgit {
        #     url = "https://framagit.org/adfaure/simgrid/";
        #     rev = "09212820ce67437a2ba2c7ce65c97cc0d721ca24";
        #     sha256 = "sha256:0nnd63lbwkzaf14cdmgs2lz9cfrs8n7ngyid56xpkhyz6q1jd5py";
        #   };
        #   # patches = [ ./tracing.pt ];
        #   doCheck = false;
        # }))
        valgrind
        custom_openblas
        # kapack.openmpi
        openmpi
        clang-tools
        ninja
        pkg-config
        # akypuera
        # I might not need this: https://github.com/xianyi/OpenBLAS/wiki/faq#whatblas
        openblas
      ];
    };

  };

in
  shells.smpi
