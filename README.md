# gemmpi

Implementation of the outer product algorithm with mpi.

# Build

The best way to build the application is using nix.

```
nix-shell shell.nix
ninja
```
The algorithm perform the multiplication of AxB on multicores using mpi.

```
A:
<-----------matrice len--------->
<----blocklen--->
+---+---+---+---+---------------+
|   |   |   |   |               |
|   |   |   |   |               |
|   |   |   |   |               |
|   |   |   |   |               |
|<---subdivis-->|               |
|               |               |
|               |               |
+---------------+---------------+
|               |               |
|               |               |
|               |               |
|               |               |
|               |               |
|               |               |
|               |               |
+---------------+---------------+

B:
<-----------matrice len--------->
<----blocklen--->
+---------------+---------------+
|               |               |
|---------------|               |
|               |               |
|---------------|               |
|               |               |
|---------------|               |
|               |               |
+---------------+---------------+
|               |               |
|               |               |
|               |               |
|               |               |
|               |               |
|               |               |
|               |               |
+---------------+---------------+
```
