#include <argp.h>
#include <assert.h>
#include <cblas.h>
#include <errno.h>  // for errno
#include <limits.h> // for INT_MAX
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#ifdef GEMSMPI
#include <simgrid/engine.h>
#endif //

FILE *f = 0; // fopen(filename, "a+");
int rank_gbl;
// Changing default values of enum
enum GempiMatrixLayout {
  GempiColMajor = 0,
  GempiRowMajor = 1,
};

void dump_matrix(char *filename, double *matrix, int nb_line, int nb_col);
double *load_matrix(char *matrix_file, enum GempiMatrixLayout layout,
                    int block_id, int total_block, int block_len_row,
                    int block_len_col);

void generate_matrix_to_file(char *filename, long rows, long cols);
void init_matrix(double *matrix, int row, int col);
void generate_random_matrix(double *A, int shift, enum GempiMatrixLayout layout,
                            int row, int col);

// Log the matrix into a file formated as "rank_matrix.txt"
void log_matrix(int, double *matrix, int n, int m);

typedef unsigned long long timestamp_t;
timestamp_t get_timestamp(void);
timestamp_t get_time();

#ifdef GEMSMPI

// Functions from Tom model
void smpi_execute_dgemm(int M, int N, int K);
double compute_mu(double M, double N, double K);
double compute_sigma(double M, double N, double K);
double dgemm_time(double M, double N, double K);

int get_nodeid(void);
int get_cpuid(void);
double random_normal(void);
double random_halfnormal(void);
double random_halfnormal_shifted(double exp, double std);
void smpi_execute_normal(double mu, double sigma);
void smpi_execute_normal_size(double mu, double sigma, double size);

//#define cblas_dgemm(layout, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc)  ({\
//    double mnk = (double)(M) * (double)(N) * (double)(K);\
//    double mn =  (double)(M) * (double)(N);\
//    double mk =  (double)(M) * (double)(K);\
//    double nk =  (double)(N) * (double)(K);\
//    double raw_duration = 2.844700e-07 + 6.317136e-11*mnk + 1.489053e-10*mn + 2.107985e-09*mk + 3.332944e-09*nk;\
//    double sigma = 1.087202e-07 + 2.976703e-12*mnk + 8.365868e-12*mn + 1.528598e-10*mk + 9.931248e-11*nk;\
//    double noise = random_halfnormal_shifted(0, sigma);\
//    double injected_duration = raw_duration + noise;\
//    smpi_execute_dgemm(M, N, K);\
//})

#define cblas_dgemm(layout, TransA, TransB, M, N, K, alpha, A, lda, B, ldb,    \
                    beta, C, ldc)                                              \
  ({ smpi_execute_dgemm(M, N, K); })

#endif // GEMSMPI

#define LOG(arg, fd, fmt, ...) ({ if (arg) { fprintf(fd, fmt, ##__VA_ARGS__); } })

const char *argp_program_version = "2.0.0";
const char *argp_program_bug_address = "adrien.faure@protonmail.com";
static char args_doc[] = "dimension";
/* Program documentation. */
static char doc[] =
    "Outer product multiplication benchmark\n"
    "The matrix are divided in blocks among the different mpi ranks.\n"
    "A:\n"
    "<-----------matrice len--------->\n"
    "<---block-len--->\n"
    "+---+---+---+---+---------------+\n"
    "|   |   |   |   |               |\n"
    "|   |   |   |   |               |\n"
    "|   |   |   |   |               |\n"
    "|   |   |   |   |     rank-1    |\n"
    "|   | rank-0|   |               |\n"
    "|   |   |   |   |               |\n"
    "|   |   |   |   |               |\n"
    "+---------------+---------------+\n"
    "|               |               |\n"
    "|               |               |\n"
    "|               |               |\n"
    "|    rank-2     |     rank-3    |\n"
    "|               |               |\n"
    "|               |               |\n"
    "|               |               |\n"
    "+---------------+---------------+\n"
    "B:\n"
    "<-----------matrice len--------->\n"
    "<----blocklen--->\n"
    "+---------------+---------------+\n"
    "|               |               |\n"
    "|---------------|               |\n"
    "|               |               |\n"
    "|---------------|               |\n"
    "|               |               |\n"
    "|---------------|               |\n"
    "|               |               |\n"
    "+---------------+---------------+\n"
    "|               |               |\n"
    "|               |               |\n"
    "|               |               |\n"
    "|               |               |\n"
    "|               |               |\n"
    "|               |               |\n"
    "|               |               |\n"
    "+---------------+---------------+\n";

/* The options we understand. */
static struct argp_option options[] = {
    {"subblock", 's', "SUBLOCKS", 0,
     "Number of block subdivisions(default to 1)."},
    {"max_loop", 'l', "LOOPS", 0,
     "Number of total loops to perform (The program stops when l is reached)."},
    {"broadcast", 'B', "Broadcast", 0,
     "Number of time to repeat a broadcast."},
    {"single_process", 1, 0, 0, "The program should run without MPI."},
    {"generate_matrix", 'g', 0, 0,
     "Generate a matrix of <dimension> into FILE and exit."},
    {"output", 'o', "FILE", 0,
     "output file either for -g or --no-mpi results."},
    {"progress", 'p', "FILE", 0,
     "progress file logging monotonic time"},
    {"matrice_a", 'a', "FILE", 0, "file to load matrix A from."},
    {"verbose", 'V', 0, 0, "Log into debug info."},
    {"matrice_b", 'b', "FILE", 0, "file to load matrix B from."},
    {"result_file", 'r', "FILE", 0,
     "If this option is given, the resulting matrix C will be written to the "
     "filename provided concatenated with the mpi rank id."},
    {0, 0, 0, 0}};
/* Used by main to communicate with parse_opt. */
struct arguments {
  char *args[1]; /* arg1 */
  int silent, verbose, nb_subblock, gen_a, gen_b, single_process,
      generate_matrix, write_results_flag, log_progress, broadcast_repeat, max_loop;
  char *file_matrix_a, *file_matrix_b;
  char *output_file, *write_results, *progress_file;
};

/* Parse a single option. */
static error_t parse_opt(int key, char *arg, struct argp_state *state) {
  char *p;
  /* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
  struct arguments *arguments = state->input;
  int path_a = 0, path_b = 0;
  switch (key) {
  case 1:
    arguments->single_process = 1;
    break;
  case 'o':
    arguments->output_file = arg;
    break;
  case 'p':
    arguments->progress_file = arg;
    arguments->log_progress = 1;
    break;
  case 'V':
    arguments->verbose = 1;
    break;
  case 'a':
    arguments->file_matrix_a = arg;
    arguments->gen_a = 0;
    break;
  case 'b':
    arguments->file_matrix_b = arg;
    arguments->gen_b = 0;
    break;
  case 's':
    // The second is the number of subdivision
    arguments->nb_subblock = strtol(arg, &p, 10);
    break;
  case 'B':
    arguments->broadcast_repeat = strtol(arg, &p, 10);
    break;
  case 'r':
    arguments->write_results = arg;
    arguments->write_results_flag = 1;
    break;
  case 'g':
    arguments->generate_matrix = 1;
    break;
  case 'l':
    arguments->max_loop = strtol(arg, &p, 10);
    break;
  case ARGP_KEY_ARG:
    if (state->arg_num >= 1)
      /* Too many arguments. */
      argp_usage(state);

    arguments->args[state->arg_num] = arg;

    break;

  case ARGP_KEY_END:
    if (state->arg_num < 1)
      /* Not enough arguments. */
      argp_usage(state);
    break;

  default:
    return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

/* Our argp parser. */
static struct argp argp = {options, parse_opt, args_doc, doc};

int main(int argc, char *argv[]) {
  struct arguments arguments;

  /* Default values. */
  arguments.log_progress = 0;
  arguments.verbose = 0;
  arguments.nb_subblock = 1;
  arguments.broadcast_repeat = 1;
  arguments.generate_matrix = 0;
  arguments.single_process = 0;
  arguments.gen_a = 1;
  arguments.gen_b = 1;
  arguments.progress_file = NULL;
  arguments.output_file   = "/dev/null";
  arguments.file_matrix_a = "none";
  arguments.file_matrix_b = "none";
  arguments.write_results = "/dev/null";
  arguments.write_results_flag = 0;
  arguments.max_loop = 0;
  /* get time */
  struct timespec ts;


  /* Parse our arguments; every option seen by parse_opt will
     be reflected in arguments. */
  argp_parse(&argp, argc, argv, 0, 0, &arguments);
  long matrix_len = strtol(arguments.args[0], NULL, 10);
  FILE *verbose_fd = NULL, *progress_fd = NULL;

  if (arguments.generate_matrix) {
    generate_matrix_to_file(arguments.output_file, matrix_len, matrix_len);
    return 0;
  }

  // fixed seed
  srand(42);
  timestamp_t time_t1, time_t2;
  // The first parameter is the initial matrice size

  // The second is the number of subdivision
  double nb_block_subdivision = (double)arguments.nb_subblock;

  int q;
  int row_color;
  int col_color;

  MPI_Comm row_comm, col_comm;
  int world_rank, world_size;
  int row_rank, row_size;
  int col_rank, col_size;

  // Initialize the MPI environment
  MPI_Init(NULL, NULL);

  // Get the rank and size in the original communicator
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // Fill the gbl.
  rank_gbl = world_rank;

  if (arguments.verbose) {

    char log_file[80];
    sprintf(log_file, "%s.%d.txt", arguments.output_file, world_rank);

    verbose_fd = fopen(log_file, "w");
    if (verbose_fd == NULL) {
      printf("Error opening file: %s!\n", arguments.output_file);
      exit(1);
    }

  }

  if (arguments.progress_file != NULL) {
    char log_file[80];
    sprintf(log_file, "%s.%d.txt", arguments.progress_file, world_rank);

    progress_fd = fopen(log_file, "w");
    if (progress_fd == NULL) {
      printf("Error opening file: %s!\n", arguments.output_file);
      exit(1);
    }
  }

  LOG(arguments.verbose, verbose_fd, "matrice dimension = %ld\nnb div: %d\ngen matrix ? %d\nOUTPUT_FILE = "
       "%s\nfile A: %s, file B: %s\n",
       matrix_len, arguments.nb_subblock, arguments.generate_matrix,
       arguments.output_file, arguments.file_matrix_a,
       arguments.file_matrix_b);

  char filename[80];
  char result_filename[80];
  if(arguments.write_results_flag){
    // Fil the name of the result file
    sprintf(result_filename, "%s.%d.txt", arguments.write_results, world_rank);
  }

  LOG(arguments.verbose, verbose_fd, "write res to %s %s\n", result_filename, arguments.write_results);

  double whole_matrice = matrix_len * matrix_len;
  // The size of the block of the matrice
  double block_size = whole_matrice / world_size;
  double block_len = sqrt(block_size);
  double division_len = floor(block_len / nb_block_subdivision);
  double division_size = block_len * division_len;

  // alpha and beta needed for the cblas_dgemm.
  double alpha = 1.0, beta = 1.0;

  // A, B, C represent each of the matrix block of the rank.
  // C will hold the result matrix.
  double *A, *B, *C;

  // q is the sqare roots of the number of process, it
  // defines the number of iterations necessary.
  // for the moment, the algorithm only supports world_size wich are perfect
  // sqared values.
  q = sqrt(world_size);
  LOG(arguments.verbose, verbose_fd, "nb_subdivsion: %f, block_len %f, division_len: %f, world_dise: %d, %f\n", nb_block_subdivision, block_len, division_len, world_size, (nb_block_subdivision * block_len * division_len * world_size));

  assert(whole_matrice ==
             (nb_block_subdivision * block_len * division_len * world_size) &&
         "failed to subdivise matrix");

  // Unperfect cutting is not handled
  assert(fmod(whole_matrice, world_size) == 0 &&
         "Matrice size cannot be divided by world_size");

  // Not perfect cutting is not handled
  assert(fmod(whole_matrice, world_size) == 0 &&
         "Matrice size cannot be divided by world_size");

  // We do not handle the case where the number of process is not a perfect
  // square. It results, to bad root bcast.
  assert((sqrt(world_size) == world_size / sqrt(world_size)) &&
         "Number of rank is not a perfect square");

  LOG(arguments.verbose, verbose_fd, "Assert controled\n");

  row_color = world_rank / q; // Determine color based on row
  col_color = world_rank % q; // Determine color based on col

  // Split the communicator based on the color and use the
  // original rank for ordering
  MPI_Comm_split(MPI_COMM_WORLD, col_color, world_rank, &col_comm);
  MPI_Comm_split(MPI_COMM_WORLD, row_color, world_rank, &row_comm);

  // Get the rank of the process in the row_comm
  // row_rank now holds its col number
  MPI_Comm_rank(row_comm, &row_rank);
  MPI_Comm_size(row_comm, &row_size);

  // Get the rank of the process in the col_comm
  // col_rank now holds its col number
  MPI_Comm_rank(col_comm, &col_rank);
  MPI_Comm_size(col_comm, &col_size);

  LOG(arguments.verbose, verbose_fd, "Communicators intialized\n");

  // If the number of subdivision is more than one, we will need smaller buffers
  // than the size of a block.
  // TODO Check modulo
  double division_len_extended = floor(block_len / nb_block_subdivision) + 1;

  time_t1 = get_timestamp();
#ifdef GEMSMPI
  C = SMPI_SHARED_MALLOC(block_len * block_len * sizeof(double));
#else
  C = malloc(block_len * block_len * sizeof(double));
#endif // GEMSMPI

  LOG(arguments.verbose, verbose_fd, "blklen: %f %f\n", block_len, block_len);
  LOG(arguments.verbose, verbose_fd, "[%d]: division_len: %f, division_len_extended: %f, block_len: %f\n", world_rank, division_len, division_len_extended, block_len);

  init_matrix(C, block_len, block_len);

  // If no input files are given, we generate matrixes.
  if (arguments.gen_a) {
    #ifdef GEMSMPI
      A = C; // SMPI_SHARED_MALLOC(block_len * block_len * sizeof(double));
    #else
      A = malloc(block_len * block_len * sizeof(double));
    #endif // GEMSMPI
    generate_random_matrix(A, world_rank, GempiColMajor, block_len, block_len);
  } else {
    A = load_matrix(arguments.file_matrix_a, GempiColMajor, world_rank, q,
                    block_len, block_len);
  }

  if (arguments.gen_b) {
    #ifdef GEMSMPI
      B = C;// SMPI_SHARED_MALLOC(block_len * block_len * sizeof(double));
    #else
      B = malloc(block_len * block_len * sizeof(double));
    #endif // GEMSMPI
    generate_random_matrix(B, world_rank, GempiRowMajor, block_len, block_len);
  } else {
    B = load_matrix(arguments.file_matrix_b, GempiRowMajor, world_rank, q,
                    block_len, block_len);
  }

  if (arguments.single_process | world_size == 1) {

    // we run it sequentialy if only one processors/rank is involved.
    cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, block_len, block_len,
                block_len, alpha, A, block_len, B, block_len, beta, C,
                block_len);
if (arguments.write_results_flag) {
      dump_matrix(result_filename, C, block_len, block_len);
      // log_matrix(0, C, block_len, block_len);
    }

    MPI_Comm_free(&row_comm);
    MPI_Comm_free(&col_comm);
    MPI_Finalize();

    free(A);
    free(B);
    free(C);

    return 0;
  }

#ifdef GEMSMPI
  double *bufferA = SMPI_SHARED_MALLOC(division_len_extended * block_len *
                                       sizeof(double));
  double *bufferB = bufferA; // SMPI_SHARED_MALLOC(division_len_extended * block_len *
                             //          sizeof(double));
#else
  double *bufferA = malloc(division_len_extended * block_len * sizeof(double));
  double *bufferB = malloc(division_len_extended * block_len * sizeof(double));
#endif // GEMSMPI
  init_matrix(bufferA, division_len_extended, block_len);
  init_matrix(bufferB, division_len_extended, block_len);

  int number_iteration = q * nb_block_subdivision;
  timestamp_t start = MPI_Wtime();
  double overedge = fmod(block_len, nb_block_subdivision);
  double division_len_loop = division_len;

  LOG(arguments.verbose, verbose_fd, "number_iteration of iteration max: %d\n", number_iteration);

  if (arguments.max_loop != 0) {
    if (arguments.max_loop > number_iteration || arguments.max_loop < 0) {
      assert(0 == 1 && "Invalid max_loop value");
    }
    LOG(arguments.verbose, verbose_fd, "Number of iterations reduced to : %d\n", arguments.max_loop);
    number_iteration = arguments.max_loop;
  }

  for (int k = 0; k < number_iteration; ++k) {
    // We find the id of the block subvivision
    int id_division;
    if (k == 0) {
      id_division = 0;
    } else {
      id_division = k / q;
    }

    double division_size_loop = block_len * division_len;
    int subdivision_id = k / q;
    int subdivision_start_idx = block_len * division_len_loop * id_division;
    int root_col = 0, root_row = 0;

    if (id_division < overedge) {
      division_len_loop++;
    }

    root_col = k % q;
    root_row = k % q;

#ifdef GEMSMPI
  printf("trace,%d,bcast,begin,%d,%f\n", world_rank, k, simgrid_get_clock());
#else
  clock_gettime(CLOCK_MONOTONIC, &ts);
  LOG(arguments.log_progress, progress_fd ,"%d,bcast,begin,%d,%ld.%09ld\n", world_rank, k, ts.tv_sec, ts.tv_nsec);
#endif // GEMSMPI
    LOG(arguments.verbose, verbose_fd, "bcast (2x) of %f\n", division_size_loop);
    // We artifficially increase the communications by repeating bcast in loop.
    // passing once in this loop is sufficient.
    for(int bcast_rep = 0; bcast_rep < arguments.broadcast_repeat; ++bcast_rep) {
      if (row_rank == root_row) {
        MPI_Bcast(&(A[subdivision_start_idx]), division_size_loop, MPI_DOUBLE,
                  root_row, row_comm);
      } else {
        MPI_Bcast(bufferA, division_size_loop, MPI_DOUBLE, root_row, row_comm);
      }
      if (col_rank == root_col) {
        MPI_Bcast(&B[subdivision_start_idx], division_size_loop, MPI_DOUBLE,
                  root_col, col_comm);
      } else {
        MPI_Bcast(bufferB, division_size_loop, MPI_DOUBLE, root_col, col_comm);
      }
    }
#ifdef GEMSMPI
    printf("trace,%d,bcast,end,%d,%f\n", world_rank, k, simgrid_get_clock());
    printf("trace,%d,comp,begin,%d,%f\n", world_rank, k, simgrid_get_clock());
    printf("trace,%d,comp,size,%f,%f\n", world_rank, block_len*block_len*division_len, simgrid_get_clock());
#else
  clock_gettime(CLOCK_MONOTONIC, &ts);
  LOG(arguments.log_progress, progress_fd ,"%d,bcast,end,%d,%ld.%09ld\n", world_rank, k, ts.tv_sec, ts.tv_nsec);

  clock_gettime(CLOCK_MONOTONIC, &ts);
  LOG(arguments.log_progress, progress_fd ,"%d,comp,begin,%d,%ld.%09ld\n", world_rank, k, ts.tv_sec, ts.tv_nsec);
#endif // GEMSMPI

    LOG(arguments.verbose, verbose_fd, "mnk: %f,%f,%f\n", block_len, block_len, division_len);
    if (col_rank == root_col && row_rank == root_row) {
       cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, block_len,
                   block_len, division_len, alpha, &A[subdivision_start_idx],
                   block_len, &B[subdivision_start_idx], block_len, beta,
                   C, block_len);
    } else if (col_rank == root_col) {
      cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, block_len,
                  block_len, division_len, alpha,
                  bufferA, block_len,
                  &B[subdivision_start_idx], block_len, beta, C,
                  block_len);
    } else if (row_rank == root_row) {
      cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, block_len,
                  block_len, division_len, alpha, &A[subdivision_start_idx],
                  block_len, bufferB, block_len, beta,
                  C, block_len);
    } else {
      cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, block_len,
                  block_len, division_len, alpha,
                  bufferA, block_len,
                  bufferB, block_len, beta,
                  C, block_len);
    }
#ifdef GEMSMPI
  printf("trace,%d,comp,end,%d,%f\n", world_rank, k, simgrid_get_clock());
#else
  clock_gettime(CLOCK_MONOTONIC, &ts);
  LOG(arguments.log_progress, progress_fd ,"%d,comp,end,%d,%ld.%09ld\n", world_rank, k, ts.tv_sec, ts.tv_nsec);
#endif // GEMSMPI

  }

  MPI_Comm_free(&row_comm);
  MPI_Comm_free(&col_comm);

  if (world_rank == 0) {
    time_t2 = get_timestamp();
    timestamp_t elapsed = MPI_Wtime();
  }

  if (arguments.write_results_flag) {
    dump_matrix(result_filename, C, block_len, block_len);
  }

#ifdef GEMSMPI
  SMPI_SHARED_FREE(bufferA);
  // SMPI_SHARED_FREE(bufferB);
  // SMPI_SHARED_FREE(A);
  // SMPI_SHARED_FREE(B);
  SMPI_SHARED_FREE(C);
  printf("trace,%d,mpi,end,%d,%f\n", world_rank, -1, simgrid_get_clock());
#else
  clock_gettime(CLOCK_MONOTONIC, &ts);
  LOG(arguments.log_progress, progress_fd ,"%d,mpi,end,%d,%ld.%09ld\n", world_rank, -1, ts.tv_sec, ts.tv_nsec);

  free(bufferA);
  free(bufferB);
  free(A);
  free(B);
  free(C);
#endif // GEMSMPI

  MPI_Finalize();

  if (arguments.verbose)
    fclose(verbose_fd);

  return 0;
}

void generate_random_matrix(double *A, int shift, enum GempiMatrixLayout layout,
                            int row, int col) {
  if (layout == GempiRowMajor) {
    for (int o = 0; o < row * col; ++o) {
      A[o] = o + 1; // o+(shift*row*col);
    }
  }
}

void init_matrix(double *matrix, int row, int col) {
  for (int o = 0; o < row * col; ++o) {
    matrix[o] = 0;
  }
}

void generate_matrix_to_file(char *filename, long rows, long cols) {
  printf("gen matrix of sixe %ldx%ld to %s\n", rows, cols, filename);
  double *M = malloc(rows * cols * sizeof(double));
  generate_random_matrix(M, 0, GempiRowMajor, rows, cols);
  dump_matrix(filename, M, rows, cols);
  free(M);
}

double *load_matrix(char *matrix_file, enum GempiMatrixLayout layout,
                    int block_id, int total_block, int block_len_row,
                    int block_len_col) {

  int total_rows, total_cols, ret;
  double *T = NULL, skip;

  // clean previous errors
  errno = 0;

  FILE *datafile = fopen(matrix_file, "r");
  if (datafile == NULL) {
    printf("Error opening file: %s!\n", matrix_file);
    exit(1);
  }

  ret = fscanf(datafile, "%dx%d\n", &total_rows, &total_cols);
  if (errno != 0) {
    perror("fail reading matrix size:");
  }

  int block_line = ((double)block_id) / total_block;
  int block_col = fmod(block_id, total_block);

  int block_line_first_id = block_line * total_cols * block_len_row,
      block_line_last_id = (1 + block_line) * total_cols * block_len_row;

  int block_col_first_id = (block_col * block_len_col),
      block_col_last_id = (block_col * block_len_col) + block_len_col;

  T = (double *)malloc(sizeof(double) * block_len_row * block_len_col);
  printf("load_matrix size: %d, %d\n", block_len_row, block_len_col);

  for (int o = 0; o < total_rows; ++o) {
    if ((o * total_cols) >= block_line_first_id &
        (o * total_cols) < block_line_last_id) {
      for (int b = 0; b < total_cols; ++b) {
        if (b >= block_col_first_id & b < block_col_last_id) {
          // printf("blk: %d, read[%d,%d], \n", block_id, o, b);
          int shift_o = o - block_line_first_id / total_rows,
              shift_b = b - block_col_first_id;
          double val;
          if (layout == GempiColMajor) {
            ret = fscanf(datafile, "%lf,", &val);
            if (ret == 1) {
              T[(shift_b * block_len_col) + shift_o] = val;
            } else if (errno != 0) {
              perror("fail reading matrix element:");
            }
          } else {
            ret = fscanf(datafile, "%lf,", &val);
            if (ret == 1) {
              T[(shift_o * block_len_col) + shift_b] = val;
            } else if (errno != 0) {
              perror("fail reading matrix element:");
            }
          }
        } else {
          ret = fscanf(datafile, "%lf,", &skip);
          if (errno != 0) {
            perror("fail skip elem:");
          }
        }
      }
    } else {
      for (int b = 0; b < total_cols; ++b) {
        ret = fscanf(datafile, "%lf,", &skip);
        if (errno != 0) {
          perror("scanf:");
        }
      }
    }
    ret = fscanf(datafile, "\n");
    if (errno != 0) {
      perror("fail skip breakline:");
    }
  }
  fclose(datafile);
  return T;
}

void dump_matrix(char *filename, double *matrix, int nb_line, int nb_col) {
  FILE *f = fopen(filename, "w");
  if (f == NULL) {
    printf("Error opening file!\n");
    exit(1);
  }

  fprintf(f, "%dx%d\n", nb_line, nb_col);
  for (int i = 0; i < nb_line; i++) {
    for (int j = 0; j < nb_col; j++) {
      fprintf(f, "%.2f,", matrix[(i * nb_col) + (j % nb_col)]);
    }
    fprintf(f, "\n");
  }
  fclose(f);
}

void log_matrix(int rank, double *matrix, int nb_line, int nb_col) {
  fprintf(f, ";%dX%d:\n", nb_line, nb_col);
  for (int i = 0; i < nb_line; i++) {
    for (int j = 0; j < nb_col; j++) {
      fprintf(f, "[%.2f]", matrix[(i * nb_col) + (j % nb_col)]);
    }
    fprintf(f, "\n");
  }
}

timestamp_t get_time() {
#ifdef HAVE_CLOCKGETTIME
  struct timespec tp;
  clock_gettime(CLOCK_REALTIME, &tp);
  return (tp.tv_sec * 1000000000 + tp.tv_nsec);
#elif HAVE_GETTIMEOFDAY
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return (tv.tv_sec * 1000000 + tv.tv_usec) * 1000;
#endif
}

timestamp_t get_timestamp(void) {
  static timestamp_t start = 0;
  if (start == 0) {
    start = get_time();
    return 0;
  }
  return get_time() - start;
}

#ifdef GEMSMPI

// Blas model from Tom Cornebize
// https://github.com/Ezibenroc/hpl/blob/heterogeneous_polynomial_dgemm/src/blas/HPL_dgemm.c#L131-L496
int get_nodeid(void) {
  static int my_rank = -1;
  static int my_node = -1;
  if (my_rank < 0) {
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    char hostname[100];
    int max_size;
    MPI_Get_processor_name(hostname, &max_size);
    int i = 0;
    while ((hostname[i] < '0' || hostname[i] > '9') && i < max_size) {
      i++;
    }
    int j = i;
    while (hostname[j] >= '0' && hostname[j] <= '9' && j < max_size) {
      j++;
    }
    assert(j < max_size);
    hostname[j] = '\0';
    my_node = atoi(&hostname[i]);
  }
  return my_node;
}

/*
 * In Dahu@G5K, there are two CPUs per node.
 * This function computes the ID of the CPU, supposing that the ranks are split
 * in even/odd. See the output of lstopo to verify this hypothesis. Note: this
 * function does *not* suppose that all the nodes of Dahu are used or that the
 * mapping is done in order. It uses the hostname to get the right ID. If we
 * made these assumptions, returning 2*(rank/32) + rank%2 would be equivalent.
 */
int get_cpuid(void) {
  static int my_rank = -1;
  static int my_cpu = -1;
  static int my_node = -1;
  if (my_rank < 0) {
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    my_node = get_nodeid();
    // 2 CPUs per node, the ranks are split in even/odd, see the output of
    // lstopo
    my_cpu = my_node * 2 + my_rank % 2;
    //      printf("rank %d, node=%d, cpu=%d\n", my_rank, my_node, my_cpu);
  }
  return my_cpu;
}

double random_normal(void) {
  // From https://rosettacode.org/wiki/Statistics/Normal_distribution#C
  double x, y, rsq, f;
  do {
    x = 2.0 * rand() / (double)RAND_MAX - 1.0;
    y = 2.0 * rand() / (double)RAND_MAX - 1.0;
    rsq = x * x + y * y;
  } while (rsq >= 1. || rsq == 0.);
  f = sqrt(-2.0 * log(rsq) / rsq);
  return (x * f); // y*f would also be good
}

double random_halfnormal(void) {
  double x = random_normal();
  if (x < 0) {
    x = -x;
  }
  return x;
}

double random_halfnormal_shifted(double exp, double std) {
  // Here, exp and std are the desired expectation and standard deviation.
  // We compute the corresponding mu and sigma parameters for the normal
  // distribution.
  double mu, sigma;
  sigma = std / sqrt(1 - 2 / M_PI);
  mu = exp - sigma * sqrt(2 / M_PI);
  double x = random_halfnormal();
  return x * sigma + mu;
}

void smpi_execute_normal(double mu, double sigma) {
  double coefficient = random_halfnormal_shifted(mu, sigma);

  if (coefficient > 0) {
    smpi_execute(coefficient);
  }
}

void smpi_execute_normal_size(double mu, double sigma, double size) {
  double coefficient = random_halfnormal_shifted(mu, sigma);
  if (coefficient > 0 && size > 0) {
    smpi_execute(size * coefficient);
  }
}

double compute_mu(double M, double N, double K) {
  double mnk = M * N * K;
  double mn = M * N;
  double mk = M * K;
  double nk = N * K;
  double mu;

  mu = 5.249134e-07 + 1.604311e-09 * mk + -1.793648e-10 * mn +
       6.864500e-11 * mnk + 2.922754e-09 * nk;
  return mu;
}

double compute_sigma(double M, double N, double K) {
  double mnk = M * N * K;
  double mn = M * N;
  double mk = M * K;
  double nk = N * K;
  double sigma;

  sigma = 6.060802e-07 + 5.905245e-11 * mk + 2.927438e-11 * mn +
          7.495901e-13 * mnk + 5.780894e-11 * nk;
  return sigma;
}

double dgemm_time(double M, double N, double K) {
  double mnk = M * N * K;
  double mn = M * N;
  double mk = M * K;
  double nk = N * K;
  double mu, sigma;

  mu = compute_mu(M, N, K);
  sigma = compute_sigma(M, N, K);

  double res = mu; //+ random_halfnormal_shifted(0, sigma);
  return res;
}

void smpi_execute_dgemm(int M, int N, int K) {

  // Code callibrated for paje
  // double time = dgemm_time(M, N, K);
  // smpi_execute(time);

  // if (time > 0) {
  // TRACE_smpi_computing_in(rank_gbl, M*N*K);
  smpi_execute_flops( (double) M*N*K);
  // TRACE_smpi_computing_out(rank_gbl);
  // }
}

#endif // GEMSMPI
